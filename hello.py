from flask import Flask
from flask import render_template
app = Flask(__name__)

#@app.route("/")
#def hello():
#        return "Hello World!"

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
            return(render_template('hello.html', name=name, key="AIzaSyC_lmsOaLjIx7Ow2TZ1sBw82ju1nO1xjeU"))

if __name__ == "__main__":
        app.run()
            
